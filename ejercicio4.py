# Ejercicio 4: Asume que ejecutamos las siguientes sentencias de asignación
# --autora-- = "Valeria Guerrero"
# --email-- = "valeria.guerrero@unl.edu.ec"
ancho= 17
alto= 12.0
a=float(ancho/2)
b=float(ancho/2.0)
c=int(alto/3)
d=int(1+2*5)

print("",ancho,"/2=",a)
print("",ancho,"/2.0=",b)
print("",alto,"/3=",c)
print("1+2*5",d)
print("Gracias")
